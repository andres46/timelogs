package co.aamsis.timeLogs

import org.springframework.data.repository.CrudRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource
import org.springframework.web.bind.annotation.CrossOrigin
import java.util.*

@CrossOrigin(origins = ["http://localhost:3000"])
@RepositoryRestResource
interface ProjectRepo: CrudRepository<Project, UUID>

@CrossOrigin(origins = ["http://localhost:3000"])
@RepositoryRestResource
interface IssueRepo: CrudRepository<Issue, UUID>

@CrossOrigin(origins = ["http://localhost:3000"])
@RepositoryRestResource
interface TimeLogRepo: CrudRepository<TimeLog, UUID>
