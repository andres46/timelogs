package co.aamsis.timeLogs

import org.springframework.http.HttpStatus.NOT_FOUND
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.*
import java.time.Duration
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import java.util.*


data class TimeLogBody(
  val issueId: UUID
)

data class TimeLogBodyStop(
  val timeLogId: UUID
)

@RestController
@RequestMapping("additionalEndpoints/time_logs")
class TimeLogController (
  val service: TimeLogService
) {
  @PostMapping("/start")
  fun persist(@RequestBody body: TimeLogBody) = service.persist(body)
  @PostMapping("/stop")
  fun close(@RequestBody body: TimeLogBodyStop) = service.close(body)
}

@Service
class TimeLogService(
  val issueRepo: IssueRepo,
  val repo: TimeLogRepo
){
  fun persist(timeLogBody: TimeLogBody): TimeLog {
    val response = issueRepo.findById(timeLogBody.issueId)
    print("TimeLogService " + Date().toString() + "persist executed")
    if (!response.isPresent) {
      throw IssueNotFoundException("Issue with id: " + timeLogBody.issueId + "not found")
    }
    val timeLogEnt = TimeLog(
      issue = response.get()
    );
    return repo.save(timeLogEnt)
  }

  fun close(timeLogBodyStop: TimeLogBodyStop): TimeLog {
    val response = repo.findById(timeLogBodyStop.timeLogId)
    print("TimeLogService " + Date().toString() + "persist executed")
    if (!response.isPresent) {
      throw IssueNotFoundException("Timelog with id: " + timeLogBodyStop.timeLogId + "not found")
    }
    val timeLog = response.get()
    timeLog.endingTime =  ZonedDateTime.now()
    timeLog.duration = Duration.between(timeLog.startingTime.toInstant(), timeLog.endingTime.toInstant()).toSeconds()
    return repo.save(timeLog)
  }

}

@ResponseStatus(NOT_FOUND)
class IssueNotFoundException(private val mss: String): RuntimeException(mss)

