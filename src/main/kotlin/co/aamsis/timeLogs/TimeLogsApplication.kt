package co.aamsis.timeLogs

import org.springframework.beans.factory.annotation.Configurable
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class TimeLogsApplication

fun main(args: Array<String>) {
	runApplication<TimeLogsApplication>(*args)
}
