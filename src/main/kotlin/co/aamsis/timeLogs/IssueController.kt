package co.aamsis.timeLogs

import org.springframework.http.HttpStatus.NOT_FOUND
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.*
import java.util.*

data class IssueBody(
  val name: String,
  val externalId: Int,
  val projectId: UUID
)

@RestController
@RequestMapping("additionalEndpoints/issues")
class IssueController (
  val service: IssueService
) {
  @PostMapping
  fun persistIssue(@RequestBody body: IssueBody) = service.persistIssue(body)
}

@Service
class IssueService(
  val repo: IssueRepo,
  val projectRepo: ProjectRepo
){
  fun persistIssue(issueBody: IssueBody): Issue {
    val response = projectRepo.findById(issueBody.projectId)
    print("IssueService " + Date().toString() + "persistIssue executed")
    if (!response.isPresent){
      throw NotFoundException("Project with id: " + issueBody.projectId + "not found")
    }
    val issueEntity = Issue(
      name = issueBody.name,
      externalId = issueBody.externalId,
      project = response.get()
    )
    return repo.save(issueEntity)
  }
}

@ResponseStatus(NOT_FOUND)
class NotFoundException(private val mss: String): RuntimeException(mss)

