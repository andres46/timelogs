package co.aamsis.timeLogs

import com.fasterxml.jackson.annotation.JsonBackReference
import com.fasterxml.jackson.annotation.JsonManagedReference
import java.time.ZonedDateTime
import java.util.*
import java.util.UUID.randomUUID
import javax.persistence.*
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

@MappedSuperclass
abstract class Persistible {
  @Id val id: UUID = randomUUID()
}

interface ExternalEntity {
  val name: String
  val externalId: Int
}

@Entity(name = "Project")
@Table(name = "project")
class Project(
  @NotBlank @NotNull
  override val name: String,
  @NotBlank @NotNull
  override val externalId: Int,
  @OneToMany(
    mappedBy = "project",
    cascade = [CascadeType.ALL],
    orphanRemoval = true
  )
  @JsonBackReference
  val issues: List<Issue> = mutableListOf()
) : Persistible(), ExternalEntity


@Entity(name = "issue")
@Table(name = "issue")
data class Issue(
  @NotBlank @NotNull
  override val name: String,
  @NotBlank @NotNull
  override val externalId: Int,
  @NotNull @ManyToOne
  @JsonManagedReference
  val project: Project,
  @OneToMany(
    mappedBy = "issue",
    cascade = [CascadeType.ALL],
    orphanRemoval = true
  )
  @JsonBackReference
  val timelogs: List<TimeLog> = mutableListOf()
) : Persistible(), ExternalEntity

@Entity
@Table(name = "time_log")
data class TimeLog(
  @NotBlank @NotNull
  val startingTime: ZonedDateTime = ZonedDateTime.now(),
  var endingTime: ZonedDateTime = ZonedDateTime.now(),
  var duration: Long = 0,
  @NotNull @ManyToOne
  @JsonManagedReference
  val issue: Issue
) : Persistible()