package co.aamsis.timeLogs.configuration

import org.springframework.core.Ordered
import org.springframework.core.annotation.Order
import org.springframework.stereotype.Component
import java.io.IOException
import javax.servlet.*
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
internal class CORSFilter : Filter {
  override fun init(fc: FilterConfig) {}
  override fun doFilter(req: ServletRequest, resp: ServletResponse, chain: FilterChain) {
    val response = resp as HttpServletResponse
    val request = req as HttpServletRequest
    response.setHeader("Access-Control-Allow-Origin", "*")
    response.setHeader("Access-Control-Allow-Origin", "*")
    response.setHeader("Access-Control-Allow-Credentials", "true")
    response.setHeader("Access-Control-Allow-Methods", "PATCH,POST,GET,OPTIONS,DELETE,PUT")
    response.setHeader("Access-Control-Max-Age", "3600")
    response.setHeader("Access-Control-Allow-Headers", "access-control-allow-origin, x-requested-with, authorization, Content-Type, Authorization, credential, X-XSRF-TOKEN")
    if ("OPTIONS" == request.method) {
      response.status = HttpServletResponse.SC_OK
    } else {
      try {
        chain.doFilter(req, resp)
      } catch (e: IOException) {
        e.printStackTrace()
      } catch (e: ServletException) {
        e.printStackTrace()
      }
    }
  }

  override fun destroy() {}
}